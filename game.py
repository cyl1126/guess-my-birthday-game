from random import randint

name = input("Hi! What is your name? ")
for guess_number in range(1,6):
    month_number = randint(1,12)
    year_number = randint(1930,2010)
    print(f"Guess #{guess_number}: {name}, were you born on {month_number}/{year_number}?")
    answer = input("Yes or no? ").lower()
    if answer == "yes":
        print("Nice! I got it right!")
        exit()
    else:
        print("Hm, let me try again.")
